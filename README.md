# Codeigniter Basic Template
This is a basic website template created with CodeIgniter 3.1.9

# How to use this in your own project?
When using this template in your own project, remember to change the base_url in application/config/config.php to match your own website domain.

The CSS styles for this website template can be edited in res/styles/styles.css